# ProseMirror

Temporary fork of [ProseMirror GitHub Repository](https://github.com/ProseMirror/prosemirror)
to backport some fixes from current ProseMirror version to 0.10.1.

* ProseMirror repository — [https://github.com/ProseMirror/prosemirror](https://github.com/ProseMirror/prosemirror).
* ProseMirror website — [http://prosemirror.net/](http://prosemirror.net/)

### DO NOT USE, WILL BE REMOVED SOON!
